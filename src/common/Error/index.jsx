import React from 'react';
import { string } from 'prop-types';
import { Alert } from 'reactstrap';

const Error = ({ error }) => (
  <div>
    <Alert color="danger">{error}</Alert>
  </div>
);

Error.propTypes = {
  error: string.isRequired
};

export default Error;
