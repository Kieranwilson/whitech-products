// Custom types for prop-type checking - quote
import { number, shape, string } from 'prop-types';

export const productShape = shape({
  description: string.isRequired,
  id: number.isRequired,
  price: string.isRequired,
  product_image: string.isRequired,
  product_name: string.isRequired
});
