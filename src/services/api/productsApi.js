import productsConfig from 'config/products';
import APIUtil from './index';

const apiUtil = new APIUtil(productsConfig.PRODUCTS.HOST);

/* I keep services as a class because normally we abstract any headers / meta data
etc to the constructor also */
export class ProdcutsAPI {
  getProductListings() {
    return apiUtil.get(productsConfig.PRODUCTS.ENDPOINTS.PRODUCT_LISTINGS);
  }
}

export default new ProdcutsAPI();
