import axios from 'axios';

export default class APIUtil {
  constructor(root) {
    this.root = root;
  }

  /**
   * Head request
   * @param {string} path
   * @returns {Promise}
   */
  head(path) {
    return axios.head(`${this.root}/${path}`);
  }

  /**
   * Get request
   * @param {string} path
   * @param {object} config
   * @returns {Promise}
   */
  get(path, config = {}) {
    return axios.get(`${this.root}/${path}`, config);
  }

  /**
   * Post request
   * @param {string} path
   * @param {object} data
   * @param {object} config
   * @returns {Promise}
   */
  post(path, data, config = {}) {
    return axios.post(`${this.root}/${path}`, data, config);
  }

  /**
   * Put request
   * @param {string} path
   * @param {object} data
   * @param {object} config
   * @returns {Promise}
   */
  put(path, data, config = {}) {
    return axios.put(`${this.root}/${path}`, data, config);
  }

  /**
   * Patch request
   * @param {string} path
   * @param {object} data
   * @param {object} config
   * @returns {Promise}
   */
  patch(path, data, config = {}) {
    return axios.patch(`${this.root}/${path}`, data, config);
  }

  /**
   * Delete request
   * @param {string} path
   * @param {object} data
   * @returns {Promise}
   */
  delete(path, data) {
    let config = {
      data: data
    };

    return axios.delete(`${this.root}/${path}`, config);
  }
}
