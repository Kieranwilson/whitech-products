import React, { PureComponent } from 'react';
import {
  Container,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { curry, times, join, curryN, __ } from 'ramda';
import { number, string } from 'prop-types';
import routes from 'config/routes';
import './style.css';

const showEitherSidePages = 3;
const calcMinPage = curry((range, current) => Math.max(1, current - range));
const calcMaxPage = curry((range, current, max) =>
  Math.min(max, current + range)
);
const calcPageNumbers = curry((range, current, max) => ({
  min: calcMinPage(range, current),
  max: calcMaxPage(range, current, max)
}));
const withRangeCalcPages = calcPageNumbers(showEitherSidePages);
const combineThreeStrings = curryN(3, (...args) => join('', args));
const routeLink = combineThreeStrings(routes.BASE.PRODUCTS);

export default class ProductListFooter extends PureComponent {
  static propTypes = {
    currentPage: number.isRequired,
    maxPage: number.isRequired,
    routePostFix: string
  };

  render() {
    const { currentPage, maxPage, routePostFix } = this.props;
    const pageRanges = withRangeCalcPages(currentPage, maxPage);
    const onlyMissingPage = routeLink(__, routePostFix);
    return (
      <Container className="productListFooter">
        <Pagination
          listClassName="justify-content-end"
          aria-label="Page navigation example"
        >
          <PaginationItem
            className="productListFooter-item"
            disabled={currentPage === 1}
          >
            <PaginationLink
              tag={Link}
              to={`${onlyMissingPage(currentPage - 1)}`}
            >
              &lsaquo; Previous Page
            </PaginationLink>
          </PaginationItem>
          {times(
            i => (
              <PaginationItem
                className="productListFooter-item"
                disabled={currentPage === i + pageRanges.min}
                active={currentPage === i + pageRanges.min}
                key={i + pageRanges.min}
              >
                <PaginationLink
                  tag={Link}
                  to={`${onlyMissingPage(i + pageRanges.min)}`}
                >
                  {i + pageRanges.min}
                </PaginationLink>
              </PaginationItem>
            ),
            pageRanges.max - pageRanges.min
          )}
          <PaginationItem
            className="productListFooter-item"
            disabled={currentPage === maxPage}
          >
            <PaginationLink
              tag={Link}
              to={`${onlyMissingPage(currentPage + 1)}`}
            >
              Next Page &rsaquo;
            </PaginationLink>
          </PaginationItem>
        </Pagination>
      </Container>
    );
  }
}
