import React, { PureComponent } from 'react';
import { Card, CardHeader, CardBody, CardTitle, CardText } from 'reactstrap';
import { productShape } from 'customPropTypes';
import './style.css';

export default class ProductCard extends PureComponent {
  static propTypes = {
    product: productShape.isRequired
  };

  render() {
    const { product } = this.props;
    return (
      <Card className="productCard" key={product.id}>
        <CardHeader className="productCard-header">
          <div className="productCard-innerHeader">
            <img
              className="productCard-image"
              src={product.product_image}
              alt={product.product_name}
            />
          </div>
        </CardHeader>
        <CardBody className="productCard-body">
          <CardTitle className="productCard-name">
            {product.product_name}
          </CardTitle>
          <CardText className="productCard-desc">
            {product.description}
          </CardText>
          <CardText className="productCard-price">{product.price}</CardText>
        </CardBody>
      </Card>
    );
  }
}
