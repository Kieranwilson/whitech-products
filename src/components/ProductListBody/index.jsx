import React, { PureComponent } from 'react';
import { Row, Col, Container } from 'reactstrap';
import { arrayOf, string } from 'prop-types';
import { map } from 'ramda';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { productShape } from 'customPropTypes';
import ProductCard from 'components/ProductCard';
import './style.css';

export default class ProductListBody extends PureComponent {
  static propTypes = {
    products: arrayOf(productShape).isRequired,
    transitionKey: string
  };

  render() {
    const { products, transitionKey } = this.props;
    return (
      <Container className="productListBody">
        <hr />
        <TransitionGroup component={null}>
          <CSSTransition
            classNames="productListBody--fade"
            key={transitionKey}
            timeout={1000}
          >
            <Row className="productListBody-row">
              {map(
                product => (
                  <Col
                    sm={3}
                    key={product.id}
                    className={'productListBody-col'}
                  >
                    <ProductCard product={product} />
                  </Col>
                ),
                products
              )}
            </Row>
          </CSSTransition>
        </TransitionGroup>
      </Container>
    );
  }
}
