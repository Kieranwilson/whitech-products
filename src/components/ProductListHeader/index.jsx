import React, { PureComponent } from 'react';
import {
  Container,
  Row,
  Col,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import './style.css';
import { map } from 'ramda';
import { number, func } from 'prop-types';

const perItemsOptions = [8, 12, 16, 32, 128];
const pageVerbage = count => `${count} per page`;

export default class ProductListHeader extends PureComponent {
  static propTypes = {
    changePerPage: func.isRequired,
    perPage: number.isRequired,
    productCount: number.isRequired
  };

  state = {
    dropdownOpen: false
  };

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  };

  renderProductCountOptions() {
    const { changePerPage, perPage } = this.props;
    const { dropdownOpen } = this.state;
    return (
      <Dropdown isOpen={dropdownOpen} toggle={this.toggle}>
        <DropdownToggle
          className="productListHeader-dropdown"
          tag="span"
          onClick={this.toggle}
          data-toggle="dropdown"
          aria-expanded={dropdownOpen}
        >
          {pageVerbage(perPage)}
        </DropdownToggle>
        <DropdownMenu right>
          {map(
            amount => (
              <DropdownItem onClick={() => changePerPage(amount)} key={amount}>
                {pageVerbage(amount)}
              </DropdownItem>
            ),
            perItemsOptions
          )}
        </DropdownMenu>
      </Dropdown>
    );
  }

  render() {
    const { productCount } = this.props;
    return (
      <Container>
        <Row className="align-items-end">
          <Col>
            <h3>All Products</h3>
            {productCount} products
          </Col>
          <Col className="text-right">{this.renderProductCountOptions()}</Col>
        </Row>
      </Container>
    );
  }
}
