import React from 'react';
import { shallow } from 'enzyme';
import queryString from 'query-string';
import { slice } from 'ramda';
import { Products } from 'scenes/Products';
import ProductListBody from 'components/ProductListBody';
import sampleProducts from './sampleProducts.json';
import routes from '../../../config/routes';

describe('Products', () => {
  const requiredProps = {
    getProductListings: jest.fn(),
    location: {},
    match: { params: {} },
    history: { push: jest.fn() }
  };

  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Products {...requiredProps} />);
  });

  it('matches snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('renders loading without products', () => {
    expect(wrapper.find('Loading')).toHaveLength(1);
  });

  it('renders error', () => {
    wrapper.setProps({ error: 'test error message' });
    expect(wrapper.find('Error')).toHaveLength(1);
  });

  it('renders correctly with products', () => {
    wrapper.setProps({ products: sampleProducts });
    expect(wrapper.children()).toHaveLength(3);
  });

  it('passes the correct number of products', () => {
    wrapper.setProps({ products: sampleProducts });
    expect(wrapper.find(ProductListBody).prop('products')).toHaveLength(8);
    wrapper.setProps({ products: sampleProducts });
    wrapper.setProps({
      location: { search: `?${queryString.stringify({ perPage: 16 })}` }
    });
    expect(wrapper.find(ProductListBody).prop('products')).toHaveLength(16);
  });

  it('passes the correct products between pages', () => {
    wrapper.setProps({ products: sampleProducts });
    let matchedProducts = slice(0, 8, sampleProducts);
    expect(wrapper.find(ProductListBody).prop('products')).toEqual(
      matchedProducts
    );
    wrapper.setProps({ match: { params: { pageNumber: '2' } } });
    matchedProducts = slice(8, 16, sampleProducts);
    expect(wrapper.find(ProductListBody).prop('products')).toEqual(
      matchedProducts
    );
  });

  it('updates history correctly', () => {
    const historyFn = jest.fn();
    wrapper.setProps({
      products: sampleProducts,
      history: { push: historyFn }
    });
    let updateHistoryFn = wrapper
      .find('ProductListHeader')
      .prop('changePerPage');
    updateHistoryFn(16);
    expect(historyFn).toHaveBeenCalledWith({
      pathname: routes.BASE.PRODUCTS,
      search: '?perPage=16'
    });
  });

  it('should load products on creation', () => {
    const withFreshFn = { ...requiredProps, getProductListings: jest.fn() };
    // eslint-disable-next-line no-unused-vars
    const freshComponent = shallow(<Products {...withFreshFn} />);
    expect(withFreshFn.getProductListings).toHaveBeenCalledTimes(1);
  });
});
