import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { arrayOf, func, bool, string, shape } from 'prop-types';
import queryString from 'query-string';
import { slice } from 'ramda';
import * as ProductActions from 'store/application/products/actions';
import { productShape } from 'customPropTypes';
import Loading from 'common/Loading';
import Error from 'common/Error';
import ProductListHeader from 'components/ProductListHeader';
import ProductListBody from 'components/ProductListBody';
import ProductListFooter from 'components/ProductListFooter';
import routes from 'config/routes';

const partThereOf = (total, part) => Math.ceil(total / part);

export class Products extends PureComponent {
  static propTypes = {
    error: string,
    loading: bool,
    products: arrayOf(productShape),
    getProductListings: func.isRequired,
    location: shape({
      pathname: string
    }).isRequired,
    match: shape({
      params: shape({
        pageNumber: string
      }).isRequired
    }).isRequired,
    history: shape({
      push: func.isRequired
    }).isRequired
  };

  state = {
    productCount: null,
    startingProduct: null,
    pageNumber: null
  };

  changePerPage = perPage => {
    const { history } = this.props;
    history.push({
      pathname: routes.BASE.PRODUCTS,
      search: `?${queryString.stringify({ perPage })}`
    });
  };

  componentDidMount() {
    const { products, loading } = this.props;
    if (!products && !loading) {
      this.props.getProductListings();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.state.pageNumber !== prevState.pageNumber ||
      this.state.productCount !== prevState.productCount
    ) {
      window.scrollTo(0, 0);
    }
  }

  static getDerivedStateFromProps(props) {
    const parsedQuery = queryString.parse(props.location.search);
    const productCount = parseInt(parsedQuery.perPage, 10) || 8;
    const pageNumber = parseInt(props.match.params.pageNumber, 10) || 1;
    return {
      productCount,
      startingProduct: (pageNumber - 1) * productCount,
      pageNumber
    };
  }

  currentProducts() {
    const { productCount, startingProduct } = this.state;
    const { products } = this.props;
    return slice(startingProduct, productCount + startingProduct, products);
  }

  render() {
    const { productCount, pageNumber } = this.state;
    const {
      loading,
      error,
      products,
      location: { search, key }
    } = this.props;
    if (error) {
      return <Error error={error} />;
    }
    if (!products || loading) {
      return <Loading />;
    }
    return (
      <React.Fragment>
        <ProductListHeader
          changePerPage={this.changePerPage}
          perPage={productCount}
          productCount={products.length}
        />
        <ProductListBody
          transitionKey={key}
          products={this.currentProducts()}
        />
        <ProductListFooter
          currentPage={pageNumber}
          maxPage={partThereOf(products.length, productCount)}
          routePostFix={search}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  products: state.data.products.productListings,
  loading: state.data.products.isLoadingProducts,
  error: state.data.products.errorLoadingProducts
});

const mapDispatchToProps = dispatch => ({
  getProductListings: () => {
    dispatch(ProductActions.getAllProducts());
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Products);
