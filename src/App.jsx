import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import routes from 'config/routes';
import './App.css';
import Products from 'scenes/Products';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path={routes.ROUTER.PRODUCTS} component={Products} />
          <Route
            render={() => (
              <React.Fragment>
                <h1>404</h1>
                <p>page not found</p>
              </React.Fragment>
            )}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
