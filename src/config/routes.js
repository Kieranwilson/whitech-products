export default {
  ROUTER: {
    PRODUCTS: '/:pageNumber(\\d*)?'
  },
  BASE: {
    PRODUCTS: '/'
  }
};
