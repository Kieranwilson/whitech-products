export default {
  PRODUCTS: {
    HOST: 'https://whitechdevs.github.io',
    ENDPOINTS: {
      PRODUCT_LISTINGS: 'reactjs-test/products.json'
    }
  }
};
