import { combineReducers } from 'redux';

import { reducer as applicationReducer } from './application/reducer';

// Combine any vendor reducers with our application reducers here
// e.g redux-form
const reducers = combineReducers({
  data: applicationReducer
});

export default reducers;
