// Combine all application reducers here
import { combineReducers } from 'redux';
import products from './products/reducer';

export const reducer = combineReducers({
  products
  // e.g. products, other data related things
});
