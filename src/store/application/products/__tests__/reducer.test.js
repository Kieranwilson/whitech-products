import productsReducer from '../reducer';
import * as productTypes from '../types';

describe('Products Reducer', () => {
  const initialState = {
    isLoadingProducts: false,
    errorLoadingProducts: null,
    productListings: null,
    active_product: null
  };

  it('should return the intial state', () => {
    expect(productsReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle UPDATE_PRODUCTS', () => {
    expect(
      productsReducer(initialState, {
        type: productTypes.UPDATE_PRODUCTS,
        payload: {
          data: 'test'
        }
      })
    ).toEqual({
      ...initialState,
      productListings: 'test'
    });
  });

  it('should handle ERROR_LOADING_APPLICATION_CONTEXT', () => {
    expect(
      productsReducer(initialState, {
        type: productTypes.ERROR_LOADING_PRODUCTS,
        payload: { data: 'loading error' }
      })
    ).toEqual({
      ...initialState,
      errorLoadingProducts: 'loading error'
    });
  });

  it('should handle START_LOADING_APPLICATION_CONTEXT', () => {
    expect(
      productsReducer(initialState, {
        type: productTypes.START_LOADING_PRODUCTS
      })
    ).toEqual({
      ...initialState,
      isLoadingProducts: true
    });
  });

  it('should handle STOP_LOADING_APPLICATION_CONTEXT', () => {
    expect(
      productsReducer(
        { ...initialState, isLoadingProducts: true },
        {
          type: productTypes.STOP_LOADING_PRODUCTS
        }
      )
    ).toEqual({
      ...initialState,
      isLoadingProducts: false
    });
  });
});
