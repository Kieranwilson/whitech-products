import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import ProductsApi from 'services/api/productsApi';
import * as productsActions from '../actions';
import * as productTypes from '../types';

const initialStoreState = {
  isLoadingProducts: false,
  errorLoadingProducts: null,
  productListings: null,
  active_product: null
};

const mockResponse = {
  data: {
    testKey: 'testData'
  }
};

jest.mock('services/api/productsApi', () => ({
  __esModule: true,
  default: {
    getProductListings: () => Promise.resolve(mockResponse)
  }
}));

const middleWares = [thunk];
const mockStore = configureMockStore(middleWares);

describe('Products Actions', () => {
  it('should fetch the products and update store', () => {
    const expectedActions = [
      { type: productTypes.START_LOADING_PRODUCTS },
      {
        type: productTypes.UPDATE_PRODUCTS,
        payload: {
          data: mockResponse.data
        }
      },
      { type: productTypes.STOP_LOADING_PRODUCTS }
    ];
    const store = mockStore({ ...initialStoreState });

    return store.dispatch(productsActions.getAllProducts()).then(() => {
      //return of asyn actions
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should dispatch an error on failure', () => {
    ProductsApi.getProductListings = () => Promise.reject(new Error('failure'));
    const expectedActions = [
      { type: productTypes.START_LOADING_PRODUCTS },
      {
        type: productTypes.ERROR_LOADING_PRODUCTS,
        payload: {
          data: 'failure'
        }
      },
      { type: productTypes.STOP_LOADING_PRODUCTS }
    ];
    const store = mockStore({ ...initialStoreState });

    return store.dispatch(productsActions.getAllProducts()).then(() => {
      //return of asyn actions
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('can set the active product', () => {
    const expectedActions = [
      {
        type: productTypes.SET_ACTIVE_PRODUCT,
        payload: {
          data: 5
        }
      }
    ];
    const store = mockStore({ ...initialStoreState });

    store.dispatch(productsActions.setActiveProduct(5));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
