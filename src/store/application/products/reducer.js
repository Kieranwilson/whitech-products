import * as Types from './types';

const initialState = {
  isLoadingProducts: false,
  errorLoadingProducts: null,
  productListings: null,
  active_product: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.START_LOADING_PRODUCTS:
      return { ...state, isLoadingProducts: true };
    case Types.STOP_LOADING_PRODUCTS:
      return { ...state, isLoadingProducts: false };
    case Types.UPDATE_PRODUCTS:
      return { ...state, productListings: action.payload.data };
    case Types.ERROR_LOADING_PRODUCTS:
      return { ...state, errorLoadingProducts: action.payload.data };
    case Types.SET_ACTIVE_PRODUCT:
      return { ...state, active_product: action.payload.data };
    default:
      return state;
  }
};

export default reducer;
