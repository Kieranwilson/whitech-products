import ProductsApi from 'services/api/productsApi';
import * as Types from './types';

/**
 * @param data - Any data to post
 * @param params - FlowAlias and Action
 * @returns {Promise} create/fetch new business case
 */
export const getAllProducts = () => {
  return async dispatch => {
    try {
      dispatch({
        type: Types.START_LOADING_PRODUCTS
      });
      const response = await ProductsApi.getProductListings();

      if (!response.data.constructor === Array) {
        // eslint-disable-next-line no-console
        console.error({ response });
        throw new Error('Response received from the endpoint was invalid');
      }

      dispatch({
        type: Types.UPDATE_PRODUCTS,
        payload: {
          data: response.data
        }
      });
    } catch (err) {
      dispatch({
        type: Types.ERROR_LOADING_PRODUCTS,
        payload: {
          data:
            err.message ||
            'Error loading product listings. Please Try Again Later.'
        }
      });
    }
    dispatch({ type: Types.STOP_LOADING_PRODUCTS });
  };
};

// life insured
export const setActiveProduct = productId => {
  return {
    type: Types.SET_ACTIVE_PRODUCT,
    payload: {
      data: productId
    }
  };
};
