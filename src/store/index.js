import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxThunk from 'redux-thunk';
import rootReducer from './reducers';

let store = createStore(rootReducer, applyMiddleware(ReduxThunk));

if (process.env.NODE_ENV === 'development') {
  const composeEnhancers = composeWithDevTools({});
  store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(ReduxThunk))
  );
}

export default store;
