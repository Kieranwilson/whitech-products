# Overview

The site is actually currently deployed [through S3 with cloudfront](d1rvc9cwjtd0y0.cloudfront.net)

### Deployment
Public Site
- Update .env first
```
cd public
yarn install
serverless deploy
yarn build
serverless syncToS3
```
To get the url of the deployed public site run: `sls domainInfo`

### Pre-requisites
For deploying, you will need the serverless framework installed and your aws CLI
configured.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
