const { echo, exec } = require('shelljs');

const packages = [
  './node_modules/query-string',
  './node_modules/strict-uri-encode/'
];

echo('\nPre build starts.\n');
packages.forEach(pack =>
  exec(`babel --presets=es2015 ${pack} --out-dir ${pack}`)
);
echo('\nPre build finished.\n');
